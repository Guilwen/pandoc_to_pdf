# pandoc_to_pdf

Here is my way to generate awesome PDF from markdown files !

|Auteur|Version|Last update| License|
|:-:|:-:|:-:|:-:|
|Guilwen Meunier|v2.0|21/09/2023|[CCO](https://creativecommons.org/publicdomain/zero/1.0/legalcode) ![](https://upload.wikimedia.org/wikipedia/commons/4/43/CC_Zero_badge.svg)|

**Functions**:
- Generate beautiful PDF from markdown files.
- Fully customizable header and footer (with logo).
- Default header and footer by pagestyle: headings
- Automatic table of content
- Automatic List of figures
- Automatic footnotes
- Automatic page number
- Add your own titlepages and endpage from PDF files (using provided PDF pages, merged with pdfpages package)

***All you need to know is how to write in Markdown !*** 
No complicated latex language and template manipulation inside some strange folders deep inside your PC.

## 1/ Why Pandoc

I've been working with Obsidian and needed a simple way to export my notes into beautiful, easily customizable PDF. I decide to use Pandoc because of how big its community is and how diverse its functions are.
Furthermore, it allows me to export to epub, odf, docx, html...
Pandoc is an CLI that convert about anything to anything. IT use latex to convert to PDF, so pandoc need a suitable version of latex installed (I've test this method with both texlive-full and texlive-small). Because I don't know much about latex langage, I've use **simple ways to customize the PDF using the YAML metadata and pandoc's default PDF template** ! 

## 2/ Body Page exemple

![Here is an exemple](img/pwrmd.png)

## 3/ Usage

1. Install Pandoc (follow this [tutorial](https://pandoc.org/installing.html)) and install texlive (follow this [tutorial](https://tug.org/texlive/acquire-netinstall.html)). I recommand texlive-full (about 5GB), but you can try and use texlive-small or texlive-basic. You can open a terminal and try `pandoc --version` to check if pandoc is installed, and `pdflatex` for the latex pdf-engine's pandoc needs.

2. Install pdfpages package (using the command `tlmgr install pdfpages` in a terminal). 

3. **Add the following YAML at the start of your markdown file** (.md). You can then edit this block to your preferences. See the guide at the end for details on each variable.

    ```
    ---
    # Metadata 
    title: #(leave title blank to prevent a page before your titlepage)
    subtitle: xxx
    author: xxx
    date: xxx
    lang: xx

    # Parameters
    geometry: margin=30mm
    lof: true 
    toc: true  
    toc-depth: 2
    toc-own-page: true
    indent: true
    pagestyle: headings

    # Title Page
    include-before: |
     \includepdf[pages=-]{img/titlepage.pdf}

    # End Page
    include-after: |
     \includepdf[pages=-]{img/endpage.pdf}

    # Load packages & Define header/footer
    header-includes: |
     \usepackage{pdfpages}
     \usepackage{sectsty}
     \sectionfont{\clearpage}
     \usepackage{fancyhdr}
     \pagestyle{fancy}
     \fancyfoot[CO,CE]{\includegraphics[width=2cm]{img/logo_cstb.png}}
     \fancyfoot[LE,RO]{\thepage}
    ---
    ```

4. Make sure your markdown file have access to every image's relative path. I recommand creating a `img` folder in the same directory as your .md file and store every image there. And prefers .png over other format. It prevents from conversion problems. It means that when you want to add an image in your markdown file, you should use the following line:

    ```
    ![Your image legend](img/your_image_name.png)
    ```

5. Open a terminal (not a window powershell). Go to the directory of your file (for instance, use command `cd my/path/to/my/file` from your root directory). Then use command :

    ```
    pandoc your_file.md -o your_file.pdf
    ```

## 4/ Guide:

For a better understanding of each YAML variable, you can check this [guide](https://pandoc.org/MANUAL.html#templates).


### Metadata

Those variables aren't printed on the pdf, but are stored as metadata.

**Title** - leave title blank to prevent a page before your titlepage

**subtitle** - Add a subtitle.

**author** - Add author.

**date** - Add date. format (xx/xx/xxxx)

**lang** - Define language using IETF language tags . format "fr", "en"...

### Parameters

**geometry: margin=30mm** - Define the size of the margin (geometry by default is A4, see pandoc guide for further details)

**lof** - Boolean (true or false). Add a page with a list of figure when true.

**toc** - Boolean (true or false). Add a table of content when true.

**toc-depth** - Define the depth of the table of content (by default 2. It means h1 and h2 title will appears)

**toc-own-page** - Boolean (true or false). Add a pagebreak after the able of content.

**indent** - dunno, see pandoc guide.

**pagestyle** - control \pagestyle{}: the default article class supports plain (default), empty (no    running heads or page numbers), and headings (section titles in running heads) 

### Title Page

include-before: includepdf[pages=-]{img/titlepage.pdf}

This line use package pdfpages. It insert a pdf page as your titlepages ! So you can easily customize the titlepage using opendocument, word or any software you want. By default, it insert every page of the pdf you provided.
I recommand storing the pdf as *titlepage.pdf* in the img folder.

### End Page

include-after: includepdf[pages=-]{img/endpage.pdf}

This line use package pdfpages. It insert a pdf page as your endpages ! So you can easily customize the titlepage using opendocument, word or any software you want. By default, it insert every page of the pdf you provided.
I recommand storing the pdf as *endpage.pdf* in the img folder.

### Load packages & Define header/footer

This part load the needed packages, and define new rules for header and footer.  You can check fancyfoot [guide](https://texblog.org/2007/11/07/headerfooter-in-latex-with-fancyhdr/) if you want a better control over header and footer.
The only important part here is how to change your footer logo. If you are satisfied by the current output, just insert your logo path instead of *YOUR/LOGO/PATH/my_logo.png*. I recommand storing the logo as *footer_logo.png* in the img folder.

